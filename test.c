#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default
	char *comando;
	char *lector;
	int temperatura=0;
	int humedad=0;
	int tiempo=0;

	int t_suma = 0;
	int t_media = 0;
	int t_menor = 1000;
	int t_mayor = -254;

	int h_suma = 0;
	int h_media = 0;
	int h_menor = 101;
	int h_mayor = -1;

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	serialport_flush(fd);
	while (tiempo < 179){	
		comando ="t";
		write(fd,comando,1);
		sleep(1);
		read(fd,&temperatura,1);
		printf("La Temperatura es:  %d \n",temperatura);

		if(temperatura < t_menor) t_menor = temperatura;
		if(temperatura > t_mayor) t_mayor = temperatura;
		if(humedad < h_menor) h_menor = humedad;
		if(humedad > h_mayor) h_mayor = humedad;

		comando="h";
		sleep(1);
		write(fd,comando,1);
		sleep(1);
		read(fd,&humedad,1);
		printf("La humedad es: %d \n",humedad);
		sleep(2);
		if (tiempo % 60 == 0){
				

				printf("Temperatura media: %f\n", (float)(t_media)/12);
				printf("\tMinimo: %d\n", t_menor);
				printf("\tMayor: %d\n", t_mayor);
				printf("Humedad media: %f\n", (float)(h_media)/12);
				printf("\tMinimo: %d\n", h_menor);
				printf("\tMayor: %d\n", h_mayor);
				 t_suma = 0;
      t_media = 0;
      t_menor = 1000;
      t_mayor = -254;
 
      h_suma = 0;
      h_media = 0;
      h_menor = 101;
      h_mayor = -1;

		}
		tiempo= tiempo + 5;
		
	}
	close( fd );

	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}


